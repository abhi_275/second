-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 19, 2019 at 08:21 PM
-- Server version: 5.7.27-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-0ubuntu0.16.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `posts`
--

-- --------------------------------------------------------

--
-- Table structure for table `relation`
--

CREATE TABLE `relation` (
  `tag_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `relation`
--

INSERT INTO `relation` (`tag_id`, `blog_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 2),
(3, 3),
(6, 18),
(7, 18),
(8, 18),
(9, 19),
(1, 19),
(10, 20),
(1, 20),
(10, 21),
(11, 22),
(1, 22),
(12, 23),
(11, 23),
(13, 24),
(14, 25),
(15, 26),
(16, 27),
(17, 27),
(18, 28),
(9, 28),
(19, 29),
(20, 29),
(1, 30),
(21, 30),
(21, 31),
(22, 31),
(23, 32),
(24, 32),
(25, 33),
(1, 33),
(26, 34),
(26, 35),
(27, 36),
(28, 36),
(25, 37),
(29, 37);

-- --------------------------------------------------------

--
-- Table structure for table `spost`
--

CREATE TABLE `spost` (
  `idn` int(10) NOT NULL,
  `title` varchar(20) NOT NULL,
  `content` text NOT NULL,
  `date` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spost`
--

INSERT INTO `spost` (`idn`, `title`, `content`, `date`) VALUES
(1, 'Kerala', ' Kerala is situated in the southwestern end of the Indian subcontinent. Kerala has a long history of art and cultural heritage and foreign trade with other countries. The state with the highest literacy rate in India, is noted for its achievements in education, health, gender equality, social justice, law and order. In addition to these, the state has the lowest infant mortality rate in the country.\r\n \r\nKerala lies between the Arabian Sea in the West and the Western Ghats (Sahyadris) in the East with an area of 38863 sq km. It is one of the five states in the linguistic-cultural area known as South India. The neighbouring states of Kerala are Tamil Nadu and Karnataka. Mayyazhi (Mahe / Mahe) is a part of Pondicherry (Puducherry) and lies within Kerala. Though the Lakshadweeps in the Arabian Sea is part of the Union Territories, it has a close alliance with the linguistic and cultural heritage of Kerala.\r\n \r\nBefore the independence of India, Kerala was one of the princely states in india. Later on 1 July 1949, the princely states of Travancore and Cochin united to form the Travancore-Cochin State. Later, When the Malabar region (formerly part of Madras state) was added to the Travancore-Cochin State. The state was formed on 1 November 1956.', '2019-11-15 16-47-43'),
(2, 'Kochi ', 'Ernakulam or Cochin/Kochi is a modern city where shopping markets, cinema complexes,industrial buildings, amusement parks,marine drive, etc are located.It is also the commercial and IT hub of Kerala. From time immemorial, Arabs, Chinese, Dutch, British and Portuguese sea-farers followed the sea route to Kochi and left their impressions on the town. The Chinese fishing nets swaying in the breeze over the backwaters, the Jewish Synagogue, the Dutch Palace , the Bolghatty Palace and Portuguese architecture in Kochi enrich the heritage of Kerala.', '2019-11-15 16-58-49'),
(3, 'Kayamkulam', 'Kayamkulam is a city and a municipality in Alappuzha district of the Indian state of Kerala. It is an ancient maritime trading centre and is almost equidistant from Alappuzha town and Kollam. The city is well connected by rail and road with other cities in the region. There are two versions as to how Kayamkulam got its name. Some say that Kayamkulam gets its name from a portmanteau of two Malayalam words – kayam (sap of a spice tree) and kulam (pond)while a more reliable version is that Kayamkulam got its name from kayal (lake) and kulam (pond),since the Kayamkulam lake (Kayamkulam kayal) is as shallow as a pond (a bit exaggerated). Kayamkulam is well known for its coir, fishing and tourism industries. The town is an important township located on the backwaters of Kerala.', '2019-11-18 11-27-21'),
(18, 'dfgbdf', 'dgfbgfbgfb', '2019-11-19 18:48:48'),
(19, 'Mavelikara', 'Mavelikara is a taluk and municipality in the Alappuzha district of the Indian state of Kerala. Located in the southern part of the district on the banks of the Achankovil River. The name Mavelikara is believed to be turned out from the words Maveli the mythical king of Kerala, and Kara means land. This land is believed to be the place where king Mahabali knelt before Vamana, offering his head for Vamana to keep his feet. The town boasts about a rich historical and cultural background. The Chettikulangara Devi Temple, known for the Kumbha Bharani festival is located near the municipality.The place is home to one of the 108 Shiva temples of Kerala created by Lord Parashurama, the Kandiyoor Mahadeva Temple . It was also a major centre of trade and commerce in ancient Kerala and the erstwhile capital of the rulers of Onattukara. As a result of the close association with the Travancore Royal Family, Mavelikkara gained modern facilities well ahead of other places in the state. It is one of the oldest municipalities of the state. Even before India attained independence, Mavelikara had a super express transport service to Trivandrum. ', '2019-11-19 18:53:30'),
(20, 'Kollam', 'Kollam is a city in the state of Kerala, on Indias Malabar Coast. Its known as a trade hub and for its beaches, like lively Kollam and secluded Thirumullavaram. Sardar Vallabhbhai Patel Police Museum has artifacts tracing the history of the police force. Nearby, Ashtamudi Lake is a gateway to the Kerala backwaters, a network of waterways rich with vegetation. The striped 1902 Tangasseri Lighthouse has ocean views.', '2019-11-19 19:10:25'),
(21, 'Abcedef', 'Kollam is a city in the state of Kerala, on Indias Malabar Coast. Its known as a trade hub and for its beaches, like lively Kollam and secluded Thirumullavaram. Sardar Vallabhbhai Patel Police Museum has artifacts tracing the history of the police force. Nearby, Ashtamudi Lake is a', '2019-11-19 19:27:44'),
(22, 'Psc', 'The Kerala Public Service Commission is a body created by the Constitution of India. The Commission advise the Government on all matters relating to civil services referred to it under Article 320 (3) of the Constitution and publish notifications inviting applications for selection to various posts as per the requisitions of the appointing authorities, conduct written test and/or practical tests, physical efficiency test and interview, prepare ranked list based on the performance of the candidates and advise candidates for appointment strictly based on their merit and observing the rules of reservation as and when vacancies are reported. ', '2019-11-19 19:29:35'),
(23, 'exam', 'The Kerala Public Service Commission is a body created by the Constitution of India. The Commission advise the Government on all matters relating to civil services referred to it under Article 320 (3) of the Constitution and publish notifications inviting applications for selection to various posts as per the requisitions of the appointing authorities, conduct written test and/or practical tests, physical efficiency test and interview, prepare ranked list based on the performance of the candidates and advise candidates for appointment strictly based on their merit and observing the rules of reservation as and when vacancies are reported. ', '2019-11-19 19:29:58'),
(24, 'Article44', 'The state with the highest literacy rate in India, is noted for its achievements in education, health, gender equality, social justice, law and order. In addition to these, the state has the lowest infant mortality rate in the country. Kerala lies between the Arabian Sea in the West and the Western Ghats (Sahyadris) in the East with an area of 38863 sq km. It is one of the five states in the linguistic-cultural area known as South India. The neighbouring states of Kerala are Tamil Nadu and Karnataka. Mayyazhi (Mahe / Mahe) is a part of Pondicherry (Puducherry) and lies within Kerala. Though the Lakshadweeps in the Arabian Sea is part of the Union Territories, it has a close alliance with the linguistic and cultural heritage of Kerala. Before the independence of India, Kerala was one of the princely states in india. Later on 1 July 1949, the princely states of Travancore and Cochin united to form the Travancore-Cochin State. Later, When the Malabar region (formerly part of Madras state) was added to the Travancore-Cochin State. The state was formed on 1 November 1956. ', '2019-11-19 19:44:14'),
(25, 'article 432', 'The state with the highest literacy rate in India, is noted for its achievements in education, health, gender equality, social justice, law and order. In addition to these, the state has the lowest infant mortality rate in the country. Kerala lies between the Arabian Sea in the West and the Western Ghats (Sahyadris) in the East with an area of 38863 sq km. It is one of the five states in the linguistic-cultural area known as South India. The neighbouring states of Kerala are Tamil Nadu and Karnataka. Mayyazhi (Mahe / Mahe) is a part of Pondicherry (Puducherry) and lies within Kerala. Though the Lakshadweeps in the Arabian Sea is part of the Union Territories, it has a close alliance with the linguistic and cultural heritage of Kerala. Before the independence of India, Kerala was one of the princely states in india. Later on 1 July 1949, the princely states of Travancore and Cochin united to form the Travancore-Cochin State. Later, When the Malabar region (formerly part of Madras state) was added to the Travancore-Cochin State. The state was formed on 1 November 1956. ', '2019-11-19 19:44:29'),
(26, 'Kocchhhh', 'It is also the commercial and IT hub of Kerala. From time immemorial, Arabs, Chinese, Dutch, British and Portuguese sea-farers followed the sea route to Kochi and left their impressions on the town. The Chinese fishing nets swaying in the breeze over the backwaters, the Jewish Synagogue, the Dutch Palace , the Bolghatty Palace and Portuguese architecture in Kochi enrich the heritage of Kerala.', '2019-11-19 19:44:59'),
(27, 'City', 'The city is well connected by rail and road with other cities in the region. There are two versions as to how Kayamkulam got its name. Some say that Kayamkulam gets its name from a portmanteau of two Malayalam words ï¿½ kayam (sap of a spice tree) and kulam (pond)while a more reliable version is that Kayamkulam got its name from kayal (lake) and kulam (pond),since the Kayamkulam lake (Kayamkulam kayal) is as shallow as a pond (a bit exaggerated). Kayamkulam is well known for its coir, fishing and tourism industries. The town is an important township located on the backwaters of Kerala. ', '2019-11-19 19:45:59'),
(28, 'Mahabali', 'The name Mavelikara is believed to be turned out from the words Maveli the mythical king of Kerala, and Kara means land. This land is believed to be the place where king Mahabali knelt before Vamana, offering his head for Vamana to keep his feet. The town boasts about a rich historical and cultural background. The Chettikulangara Devi Temple, known for the Kumbha Bharani festival is located near the municipality.The place is home to one of the 108 Shiva temples of Kerala created by Lord Parashurama, the Kandiyoor Mahadeva Temple . It was also a major centre of trade and commerce in ancient Kerala and the erstwhile capital of the rulers of Onattukara. As a result of the close association with the Travancore Royal Family, Mavelikkara gained modern facilities well ahead of other places in the state. It is one of the oldest municipalities of the state. Even before India attained independence, Mavelikara had a super express transport service to Trivandrum. ', '2019-11-19 19:46:31'),
(29, 'Thiruvananthapuram', 'Thiruvananthapuram (or Trivandrum) is the capital of the southern Indian state of Kerala. Its distinguished by its British colonial architecture and many art galleries. Itâ€™s also home to Kuthira Malika (or Puthen Malika) Palace, adorned with carved horses and displaying collections related to the Travancore royal family, whose regional capital was here from the 18thâ€“20th centuries.', '2019-11-19 19:47:16'),
(30, 'Kottayam', 'Kottayam is a city in the Indian state of Kerala.It is located in central Kerala and is also the administrative capital of Kottayam district.Bordered by the lofty and mighty Western Ghats on the east and the Vembanad Lake and paddy fields of Kuttanad on the west, Kottayam is a land of unique characteristics.Panoramic backwater stretches, lush paddy fields, highlands, hills and hillocks, extensive rubber plantations, places associated with many legends and a totally literate people have given Kottayam District the enviable title:The land of letters, legends, latex and lakes. The city is an important trading center of spices and commercial crops, especially rubber. Most of Indiaâ€™s natural rubber originates from the acres of well-kept plantations of Kottayam,', '2019-11-19 19:48:10'),
(31, 'History', 'The present Kottayam district was previously a part of the erstwhile princely state of Travancore. Earlier, the Travancore state consisted of two revenue divisions viz. the southern and northern divisions, under the administrative control of a â€˜Diwan Peshkarâ€™ for each. Later in 1868 two more divisions Quilon (Kollam) and Kottayam were constituted. The fifth division Devikulam came next but only for a short period, which in course of time, was added to Kottayam. At the time of the integration of the State of Travancore and Cochin (Kochi) in 1949, these revenue divisions were renamed as districts and the Diwan peshkars gave way to District Collectors, paving the way for the birth of the Kottayam District in July 1949.', '2019-11-19 19:49:26'),
(32, 'NDMA', 'National Disaster Management Authority (NDMA) has approved a centrally sponsored scheme named Aapda Mitra, focussed on training 200 community volunteers in disaster response in Kottayam district. The said scheme aims to provide the community volunteers with the skills that they would need to respond to their communityâ€™s immediate needs in the aftermath of a disaster thereby enabling them to undertake basic relief and rescue tasks during emergency situations such as floods, flash-floods and urban flooding. KSDMA has decided to utilize this project as a platform to trigger the beginning of Civil Defence in Kerala with the approval of NDMA.', '2019-11-19 19:50:11'),
(33, 'Alappuzha', 'Alappuzha is a Land Mark between the broad Arabian sea and a network of rivers flowing into it.In the early first decade of the 20th Century the then Viceroy of the Indian Empire, Lord Curzon made a visit in the State to Alleppey, now Alappuzha. Fascinated by the Scenic beauty of the place, in joy and amazement.Thus the sobriquet found its place in the world Tourism Map. The presence of a port and a pier, criss -cross roads and numerous bridges across them, a long and unbroken sea coast might have motivated him to make this comparison.\r\nAlleppey has a wonderful past. Though the present town owes its existence to the sagacious Diwan Rajakesavadas in the second half of 18th century, district of Alappuzha figures in classified Literature. Kuttanad, the rice bowl of Kerala with the unending stretch of paddy fields, small streams and canals with lush green coconut palms , was well known even from the early periods of the Sangam age. History says Alappuzha had trade relations with ancient Greece and Rome in B.C and in the Middle Ages.', '2019-11-19 19:51:41'),
(34, 'Path', 'The history of the district in the Paleolithic age is obscure. It is presumed that the coastal taluks of Cherthala. Ambalappuzha and Karthikappally might have been under water and these areas were formed by the accumulation of silt and sand later than the other parts of the district. Kuttanad was well known even from the early periods of the Sangam age. The early Cheras had their home in Kuttanad and they were called â€˜Kuttuvansâ€™ named after this place. Some archeological antiques like stone inscriptions, historical monuments etc. found in the temples, churches, rock-out caves etc., and literacy works like', '2019-11-19 19:54:10'),
(35, 'Words', 'Apart from counting words and characters, our online editor can help you to improve word choice and writing style, and, optionally, help you to detect grammar mistakes and plagiarism. To check word count, simply place your cursor into the text box above and start typing. You ll see the number of characters and words increase or decrease as you type, delete, and edit them. You can also copy and paste text from another program over into the online editor above. The Auto Save feature will make sure you wont lose any changes while editing, even if you leave the site and come back later.', '2019-11-19 19:55:20'),
(36, '17th century', 'n the 17th century the Portugese power declined and the Dutch had a predominant position in the principalities of this district. As a result of several treaties signed between the Dutch and the Rajas of Purakkad, Kayamkulam and Karappuram, the Dutchbuilt factories and warehouses in various places of the district for storing pepper,ginger etc. In course of time they inferred in the political and cultural affairs of the district. It was at that time Maharaja Marthandavarma, the â€˜Maker of modern Travancoreâ€™ interfered in the political affairs of those principalities. The annexation of the Kingdoms of Kayamkulam, Ambalappuzha, Thekkumkur, Vadakkumkur and Karappuram to Travancore gave the Dutch a setback from the political scene of the district. Marthandavarma Maharaja had a remarkable role in the internal progress of the district. He gave special attention to the development of the district  as an administrative as well as a commercial centre. The Krishnapuram palace, which is now a protected monument of the State Archaeology department, was constructed during that period. It was at that time that the great and talented poet Kunjan Nambiar was installed in the court at Trivandrum.', '2019-11-19 19:56:15'),
(37, 'Modern aleppy', 'During the reign of Dharmaraja the district was improved by all means. Raja Kesava Das, the then Diwan of Travancore who was known as the â€˜Maker of modern Alleppeyâ€™ made Alappuzha a premier port town of Travancore. He constructed several roads and canals to improve communications and built warehouses. He gave all facilities to merchants and traders from far and near. During the reign of Balaramavarma Maharaja, VeluThampiDalava took keen interest in the development of the town and port. He brought the whole area of the island Pathiramanal under coconut cultivation and large tracts under paddy cultivation. The role of VeluThampiDalava in the development of Alappuzha is worth mentioning. In the 19th century the district attained progress in all spheres. One of the five subordinate courts opened in the state in connection with the reorganization of the judicial system by Colonel Munro was located at Mavelikkara. The credit of having the first post office and first telegraph office in the erstwhile Travancore state goes to this district. The first modern factory for the manufacture of coir mats and mattings was also established in 1859 at Alappuzha. The town Improvement Committee was set up in 1894.', '2019-11-19 20:06:08');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `tid` int(11) NOT NULL,
  `tag` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tid`, `tag`) VALUES
(1, 'kerala'),
(2, 'kochi'),
(3, 'kayamkulam'),
(6, 'abc'),
(7, 'hyth'),
(8, 'fff'),
(9, 'mavelikara'),
(10, 'kollam'),
(11, 'psc'),
(12, 'exam'),
(13, 'article'),
(14, 'art'),
(15, 'chinese'),
(16, 'city'),
(17, 'version'),
(18, 'mahabali'),
(19, 'tvm'),
(20, 'royal'),
(21, 'kottayam'),
(22, 'history'),
(23, 'ndma'),
(24, 'authority'),
(25, 'alappuzha'),
(26, 'path'),
(27, '17'),
(28, 'century'),
(29, 'modern');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `spost`
--
ALTER TABLE `spost`
  ADD PRIMARY KEY (`idn`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `spost`
--
ALTER TABLE `spost`
  MODIFY `idn` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
